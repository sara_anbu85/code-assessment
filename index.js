
 var title = require('./titles');
 var getRepeatedNumber = require('./repeated-number');
 var getNumbersInterval = require('./display-number');
  

/*Given the array Numbers that consists of X integers and always in a non-decreasing order.
Find if there’s any “most-prominent” number, being this number the number that ocurrs
more than half the length of the array.*/
var arrNumbers  = [400,400,523,523,523,523,523,523,823,93333];
getRepeatedNumber(arrNumbers);

// Print the numbers from 1 to n with a delay of 1 second each.
var messages = [1, 2, 3, 4, 5]; 
getNumbersInterval(messages);


//function that will retrieve some data from an external RESTful API.
var apiUrl = "https://jsonplaceholder.typicode.com/posts";
var apiUrlId = "https://jsonplaceholder.typicode.com/posts/28";
getTitles(apiUrlId);

 
function getTitles (apiUrl) { 
    title(apiUrl).then(function(response) {
   
    //parse the response to get JSON array
    let responseObj = JSON.parse(response);    
   
    //Check the api returns more than one title ex: Without Id
    if(responseObj.length>0) {
        //Loops the array to get the title
        for(let val of responseObj) {
            console.log(val.title);
        }
    }
    else { console.log(responseObj.title); }
    
  }, function(error) {
    console.error("Failed!", error);
  })

}