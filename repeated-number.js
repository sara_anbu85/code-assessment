/*
Pseudocode :
1. Declare 'output' variable set value -1;
2. Declare 'currVal' to hold the current value of the arrNumbers and checks for repeataion during iteration.
3. Declare 'count' variable set value '0
4. Iterate the array of arrNumbers
5. when i = 0 , sets first occurance of the number in the 'arrNumbers' to 'currVal' and the count to 1.
6. For loop on each iteration, checks 'currVal' to the remaining numbers in the 'arrNumbers', 
if "yes" then the number is repeated and so 'count' is increased to 1. if "no" then then the number is not repeated 
change the currVal to the next value in the arrNumbers. 
7. Whenever the loop encounters new number in the 'arrNumbers' i.e (currval != arrNumber[i]), Checks the 'count' > 0 this gives the previous number repeated count. 
8. 'count' > 0 is 'yes' checks the count is greater than half of count of 'arrNumbers'.
 Then the loop exits with 'break' and returns the output value.
*/


module.exports = (arrNumbers) => {

var output= -1;
var currVal = null;
var count = 0;

//Loops through array
for(let i=0;i<arrNumbers.length;i++) {
    //checks for repeat of number
    if (arrNumbers[i] != currVal) {
      if (count > 0) {
        //check count is greater than half of array Length
        if(count>(arrNumbers.length/2)) {
          output = currVal;
          break; 
        } 
      }
      currVal = arrNumbers[i];
      count = 1;
    } else {
      count = count + 1; 
    }
}
console.log(output);

}
