/* Print the numbers from 1 to n with a delay of 1 second each.
We came up with this piece of code, but for some reasons it’s printing n times “undefined”. 
Can you tell why and how you would write the code instead?

Answer : 
Reason: Javascript executes the 'for loop' iteration without waiting for the completion of the time Interval. It runs the entire
iteration before 1 second and checks the 'i' value, hence we get the value of the 'i' as 'undefined'.
Solution: When we declare the 'i' with 'let' it becomes 'block' scoped i.e. the value of the 'i' can be find 
only inside the 'for' block and not outside the block and hence each time when the iteration goes it 
finds the new value  of the 'i' and prints at time interval.

*/


module.exports = (messages) => { 
for (let i = 0; i < messages.length; i++)
{  setTimeout(function() { console.log(messages[i]);  }, 1000 * i); }
}