/*
Promise : In Javascript, promise helps to reserve place for the output. 
Promises gives 'user' response will be sent once it is ready.

PsuedoCode: 
1. 'getTitle' function return Promise which calls 'apiURL' and sends back the response through callback function.
2. Promise takes an argument with 2 functions 'resolve' and 'reject'. Resolve will handle the 'success' response and Reject 
will be called on 'Error'.
3. Create 'XMLHttpRequest' and call the api URL with verb "GET"
4. On 'Sucess' the response will be resolved and handled through 'then' function.
5. The respose output is 'text' hence converted to Javascript object using 'JSON.parse'.
6. Check the response output array count is more than 1, 'yes' then iterate the array through for loop to extract title property.
7. If 'Id' is specified than one array will be returned, display the title property.
8. Error is handled through rejected method and error will be displayed to the user.
8.Ends.

*/


var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

module.exports = (apiUrl) => {
    // Return a new promise.
    return new Promise(function(resolve, reject) {

      // Creates the HttpRequest 
      var req = new XMLHttpRequest();
      req.open('GET', apiUrl);

      req.onload = function() {

        //Check the status of the request "200" indicates success
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.responseText);         
        }
        else {          
          // If Error sets status text as error
          reject(Error(req.status));         
        }
      };
      
       // Handle  errors
      req.onerror = function() {
        reject(Error("Error Occurred"));
      };
  
      // Send the request
      req.send();
    });
  }