# EF Code Assessment

The Project contains the program given for coding assessment. 

* Program for each question is created in separate file. 

1.  *display-number.js*

2.	*titles.js*

3.	*repeated-number.js*

* All the files were included in the index.js and the methods were called and executed from the index page.

* User can test the program by passing different inputs from the index page.

* Answer for the 4th question landing page is given in separate word document “LandingPage-Saraswathi.docx”


**Additional Requirement:**


    1.	To execute the program from your local environment, download the folder and place it in a convenient path.
    2.	Enter into the folder from CLI
    3.	And at the promt type “npm intit” to initialize the project in npm
    4.	I have installed “XMLHttpRequest” for calling API through Http. You can install this package in by typing the following 
        “npm install xmlhttprequest” 
    5.	To run the Node application, type the following at the prompt 
        “npm start”
